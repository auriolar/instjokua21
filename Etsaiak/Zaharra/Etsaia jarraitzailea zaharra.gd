extends KinematicBody2D
var abiadura = 100
var posi = 0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var gravity = 800
var velocity = Vector2()
# Called when the node enters the scene tree for the first time.
func _process(delta):
	velocity.x = 0
	velocity.y = velocity.y + gravity*delta
	var posi = get_angle_to(get_tree().get_root().get_node("Node2D/Player").position)
	if 1.6 > posi and posi > -1.6:
		velocity.x = abiadura
		$Sprite1.flip_h = false
	else:
		velocity.x = -abiadura
		$Sprite1.flip_h = true
	move_and_slide(velocity)



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
