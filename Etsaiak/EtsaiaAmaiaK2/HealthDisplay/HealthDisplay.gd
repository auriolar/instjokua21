extends Node2D

var bar_red = preload("res://Etsaiak/EtsaiaAmaiaK2/HealthDisplay/barHorizontal_red.png")
var bar_green = preload("res://Etsaiak/EtsaiaAmaiaK2/HealthDisplay/green_button01.png")
var bar_yellow = preload("res://Etsaiak/EtsaiaAmaiaK2/HealthDisplay/barHorizontal_yellow.png")

onready var healthbar = $HealthBar

func _ready():
	#hide()
	if get_parent() and get_parent().get("max_health"):
		healthbar.max_value = get_parent().max_health
	healthbar.texture_progress = bar_green

func _process(delta):
	global_rotation = 0

func update_healthbar(value):
	if value < healthbar.max_value * 0.35:
		healthbar.texture_progress = bar_red
	elif value < healthbar.max_value * 0.7:
		healthbar.texture_progress = bar_yellow
	elif value <= healthbar.max_value:
		healthbar.texture_progress = bar_green
	healthbar.value = value
