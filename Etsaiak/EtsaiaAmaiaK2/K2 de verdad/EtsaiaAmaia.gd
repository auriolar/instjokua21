extends KinematicBody2D

const GRAVITY = 10
const FLOOR = Vector2(0,-1)
const FIREBALL = preload("res://Etsaiak/EtsaiaAmaiaK2/K2 de verdad/fireball.tscn")
const HEALTHDISPLAY = preload("res://Etsaiak/EtsaiaAmaiaK2/HealthDisplay/HealthDisplay.tscn")

var velocity = Vector2()
var direction = 1
var is_dead = false
var jokalaria = " "
var distancejok = " "
var timer = 0

export(int) var speed = 30
export(int) var hp = 1

func _ready():
	pass

func dead():
	hp = hp - 1
	if hp <=0:
		is_dead = true 
		velocity = Vector2(0,0)
		$AnimatedSprite.play("dead")
		$CollisionShape2D.disabled = true
	
func _physics_process(delta):
	jokalaria = get_tree().get_root().get_node("Node2D/Player").position
	distancejok = abs(position.distance_to(jokalaria))
	
	velocity.x = speed * direction
	
	if direction == 1:
		$AnimatedSprite.flip_h = false
	else: 
		$AnimatedSprite.flip_h = true
	
	$AnimatedSprite.play("Run")
	
	velocity.y += GRAVITY
	velocity - move_and_slide(velocity, FLOOR)
	
	if is_on_wall():
		direction = direction * -1
		$RayCast2D.position.x *= -1
	
	if $RayCast2D.is_colliding() == false:
		direction = direction * -1
		$RayCast2D.position.x *= -1
	
	if get_slide_count() > 0:
			for i in range(get_slide_count()):
				if "Player" in get_slide_collision(i).collider.name:
					get_slide_collision(i).collider.dead()

	if(distancejok<250): 
		if timer >= 2:
			bota() 
			print("bota")
			timer = 0

func bota():
		var fireball = FIREBALL.instance()
		fireball.norabide = direction
		get_parent().add_child(fireball)
		fireball.position = $Position2D.global_position
		fireball.position.x += direction * 50
	#	if $Timer.wait_time == 1:


func _on_Timer_timeout():
	timer += 1
	
