extends Area2D

# Marrazkia:
# https://thenounproject.com/term/minus/85540/

const SPEED = 100
var velocity = Vector2()
var norabide = 1
func _ready():
	pass 

func _physics_process(delta):
	velocity.x = norabide * SPEED * delta
	translate(velocity)
	print(velocity)
	$AnimatedSprite.play("shootMinus")

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()

func _on_Area2D_body_entered(body):
	if body.name == "Player" :
		body.dead()
	if not "Etsaia"  in body.name:
		queue_free()
