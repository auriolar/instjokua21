extends KinematicBody2D

var step = 0
export var move_speed = 100
export var anplitudea = 1

export (String, "pja", "pi", "pn") var irudia = "pja"

func _ready():
	if irudia == "pja":
		$AnimatedSprite.animation = "Plataforma"
	elif irudia == "pi": 
		$AnimatedSprite.animation = "Plataforma_Iñaki"
	else:
		$AnimatedSprite.animation = "Plataforma_Nahia"

func _physics_process(delta):
	step += delta 
	var n = cos(step * 1/anplitudea)
	
	if n < 0: 
		move_and_slide(Vector2(move_speed,0))
	elif n > 0:
		move_and_slide(Vector2(-move_speed,0))
	
	#platform.position = new_pos
