extends KinematicBody2D

export (int) var run_speed = 100
export (int) var jump_speed = -600
export (int) var gravity = 1200

var dcount = 0

var state_machine

var velocity = Vector2()
var jumping = false

func _ready():
	state_machine = $AnimationTree.get("parameters/playback")

func get_input():
	var current = state_machine.get_current_node()
	if velocity.length() > -1 and velocity.length() < 1:
		state_machine.travel("Idle")
	velocity.x = 0
	
	var right = Input.is_action_pressed("ui_right")
	var left = Input.is_action_pressed("ui_left")
	var jump = Input.is_action_pressed("ui_accept")
	
	if jump and is_on_floor():
		jumping = true
		velocity.y = jump_speed
	
	if left:
		velocity.x -= run_speed
		$Sprite.flip_h = true
		
	if right:
		velocity.x = run_speed
		$Sprite.flip_h = false
		
	if is_on_floor():
		
		if right and velocity.x > 0.5:
			state_machine.travel("Walk")
			
		if left  and velocity.x < -0.5:
			state_machine.travel("Walk")
			
	
	if not is_on_floor() and velocity.y > 0.1:
		state_machine.travel("Fall")
	if not is_on_floor() and velocity.y < 0.1:
		state_machine.travel("Jump") 
		
func _physics_process(delta):
	get_input()
	velocity.y += gravity*delta
	if jumping and is_on_floor():
		jumping = false
	velocity = move_and_slide(velocity, Vector2(0, -1))
	dcount = dcount +1
	if dcount == 3:
		autodebug()
		dcount = 0

func autodebug():
	print("VSpeed = ", velocity.y)
	print("HSpeed = ", velocity.x)
	print("TotalSpeed = ", velocity.length())
	print("Animation: ", state_machine.get_current_node())
	print("Floor? ", is_on_floor())

