extends KinematicBody2D
var abiadura = 100
var posi = 0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var gravity = 800
var velocity = Vector2()
# Called when the node enters the scene tree for the first time.
func _process(delta):
	velocity.x = 0
	velocity.y = velocity.y + gravity*delta
	var jpos = get_tree().get_root().get_node("Node2D/Player").position
	var posi = get_angle_to(jpos)
	
	#print(name,"pos ",position.x," pos2 ",jpos.x)
	#if -1.6 < posi and posi < -0.5 or 0.5 < posi and posi < 1.6:
	
	if -1.6 < posi and posi < 1.6:
		velocity.x = abiadura
		$AnimatedSprite.flip_h = false
	else:
		velocity.x = -abiadura
		$AnimatedSprite.flip_h = true
	if abs(position.x-jpos.x) < 5 && abs(position.y-jpos.y) > 100:
		velocity.x = 0
		$AnimatedSprite.flip_h = false
	if velocity.x == 0:
		$AnimatedSprite.play("idle")
	else:
		$AnimatedSprite.play("walk")
	print(velocity)
	print(abs(position.x-jpos.x) )
	move_and_slide(velocity)
	
	if get_slide_count() > 0:
		for i in range(get_slide_count()):
			if "Player" in get_slide_collision(i).collider.name:
				get_slide_collision(i).collider.dead()


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
