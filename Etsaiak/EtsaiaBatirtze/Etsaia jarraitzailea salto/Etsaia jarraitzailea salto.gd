extends KinematicBody2D
var abiadura = 100
var posi = 0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var gravity = 800
var jump_v = -500
var velocity = Vector2()
# Called when the node enters the scene tree for the first time.

func _ready():
	$RayCast2D.position.y = position.y + jump_v

func _process(delta):
	$RayCast2D.position.y = position.y + jump_v
	print($RayCast2D.position.y,position.y,$RayCast2D.is_colliding() )
	velocity.x = 0
	if is_on_wall() and is_on_floor() and $RayCast2D.is_colliding() == false:
		velocity.y = jump_v
	velocity.y = velocity.y + gravity*delta
	var posi = get_angle_to(get_tree().get_root().get_node("Node2D/Player").position)
	#print(position)
	if 1.6 > posi and posi > -1.6:
		velocity.x = abiadura
		$Sprite1.flip_h = false
	else:
		velocity.x = -abiadura
		$Sprite1.flip_h = true
	move_and_slide(velocity,Vector2(0,-1))
	
	if get_slide_count() > 0:
		for i in range(get_slide_count()):
			if "Player" in get_slide_collision(i).collider.name:
				get_slide_collision(i).collider.dead()



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
