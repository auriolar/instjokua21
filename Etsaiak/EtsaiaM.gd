extends Node2D

var step = 0
export var move_speed = 10
export var radius = 500

onready var platform = $Platform

func _physics_process(delta):
	step += delta
	var hx = $Platform.position.x
	var new_pos = Vector2()
	new_pos.x = cos(step * move_speed) * radius
	new_pos.x = sin(step * move_speed) * radius
	
	if new_pos.x < hx: 
		$Platform/AnimatedSprite.flip_h = true
	elif new_pos.x > hx: 
		$Platform/AnimatedSprite.flip_h = false
	
	platform.position = new_pos
