extends CanvasLayer

onready var globalak = get_node("/root/Globala")

func _ready():
	if globalak.mobile == false:
		for i in range(4):
			$MobileInterface.get_child(i).hide()
		

func _on_RightButton_pressed():
	Input.action_press("ui_right")

func _on_RightButton_released():
	Input.action_release("ui_right")


func _on_CrouchButton_pressed():
	Input.action_press("ui_down")

func _on_CrouchButton_released():
	Input.action_release("ui_down")
	

func _on_JumpButton_pressed():
	Input.action_press("ui_accept")

func _on_JumpButton_released():
	Input.action_release("ui_accept")


func _on_LeftButton_pressed():
	Input.action_press("ui_left")

func _on_LeftButton_released():
	Input.action_release("ui_left")
