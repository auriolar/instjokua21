extends Area2D

export(NodePath) var helmuga = "pasabidea/ateak/atea2"
var atean = false

var denbora = 0
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if  Input.is_action_pressed("ui_up") and atean:
			var p = get_tree().get_root().get_node("pasabidea/Player")
			var a = get_node(helmuga)
			atean = false
			p.position = a.position
			

func _on_Area2D_body_entered(body):
	if body.name == "Player":
		atean = true


func _on_atea_body_exited(body):
	if body.name == "Player":
		atean = false
