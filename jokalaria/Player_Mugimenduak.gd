extends KinematicBody2D

onready var globalak=get_node("/root/Globala")

export (int) var walk_speed = 175
export (int) var run_max_speed = 400
export (int) var run_acceleration = 2
export (int) var jump_speed = -550 #Salto egitean altuera maximoa
export (int) var jump_step = -80 #Saltoan zehar azelerazioa
export (int) var jump_first_step = -250 #Salto minimoa - lehenengo pausua
export (int) var jump_max_delay = 10
export (int) var gravity = 1200
export (int) var terminal_velocity = 2000
export (int) var health = 1
export (int) var bizitza = 100

var koloreak = ["#ebfefefd", "#a3fefefd", "43fefefd", "a3fefefd", "ebfefefd"]
var i = 0

var female = load("res://jokalaria/female_spritesheet.png")
var male = load("res://jokalaria/male_spritesheet.png")
var is_female = false
var logger = false

var dcount = 0
var jcount = 0
var rcount = 0
var lcount = 0

var state_machine

var velocity = Vector2()
var jumping = false
var jump_started = false
var jump_again = false
var wall_jump = false
var wall_jump_active = true

func _ready():
	wall_jump_active = globalak.wall_jump_active
	is_female = globalak.is_female
	state_machine = $AnimationTree.get("parameters/playback")
	set_process_input(true)
	
func get_input():
	var current = state_machine.get_current_node()
	var down = Input.is_action_pressed("ui_down")
	if velocity.length() > -1 and velocity.length() < 1:
		state_machine.travel("Idle")
		rcount = 0
		lcount = 0
		$PlayerHitbox_crouch.disabled = true
		$PlayerHitbox_standard.disabled = false
		if down or is_on_ceiling():
			state_machine.travel("CrouchIdle")
			$PlayerHitbox_crouch.disabled = false
			$PlayerHitbox_standard.disabled = true
	if not wall_jump:
		velocity.x = 0

	var right = Input.is_action_pressed("ui_right")
	var left = Input.is_action_pressed("ui_left")
	var jump = Input.is_action_pressed("ui_accept")

	if Input.is_action_just_released("ui_accept"):
		jump_started = false
		jump_again = false

	if jump and is_on_floor() or jump_started:
		jumping = true
		jump_started = true
		jump_again = true
		if is_on_ceiling():
			jump_started = false
		if velocity.y == 0:
			velocity.y = jump_first_step
		velocity.y = velocity.y + jump_step
		if velocity.y < jump_speed:
			velocity.y = jump_speed
			jcount = jcount +1
			if jcount >= jump_max_delay:
				jump_started = false
				jcount = 0


	if left and not right:
		if not wall_jump:
			velocity.x = -walk_speed
			rcount = 0
			lcount = lcount + 1
			velocity.x = -walk_speed + lcount*(-run_acceleration)
			if velocity.x < -run_max_speed:
				velocity.x = -run_max_speed
			$Sprite.flip_h = true

		if jump and not jump_again and not is_on_floor() and is_on_wall() and wall_jump_active:
			wall_jump = true
			lcount = max(lcount, rcount)
			rcount = max(lcount, rcount)
			velocity.y = jump_speed
			velocity.x = -velocity.x


	if right and not left:
		if not wall_jump:
			velocity.x = walk_speed
			lcount = 0
			rcount = rcount + 1
			velocity.x = walk_speed + rcount*run_acceleration
			if velocity.x > run_max_speed:
				velocity.x = run_max_speed
			$Sprite.flip_h = false

		if jump and not jump_again and not is_on_floor() and is_on_wall() and wall_jump_active:
			wall_jump = true
			lcount = max(lcount, rcount)
			rcount = max(lcount, rcount)
			velocity.y = jump_speed
			velocity.x = -velocity.x
			
	if wall_jump:
		if velocity.x < -20 and left:
			wall_jump = false
		if velocity.x > 20 and right:
			wall_jump = false


	if is_on_floor():
		wall_jump = false
		if right and velocity.x > 0.5:
			state_machine.travel("Walk")
			$StepSound.playing = false
			if is_on_ceiling() or down:
				state_machine.travel("CrouchWalk")
				$PlayerHitbox_standard.disabled = true
				$PlayerHitbox_crouch.disabled = false
				if velocity.x > ((walk_speed+run_max_speed)/2):
					velocity.x = ((walk_speed+run_max_speed)/2)
			elif velocity.x > ((walk_speed+run_max_speed)/2):
				state_machine.travel("Sprint")

		if left  and velocity.x < -0.5:
			state_machine.travel("Walk")
			$StepSound.playing = false
			if down:
				state_machine.travel("CrouchWalk")
				$PlayerHitbox_standard.disabled = true
				$PlayerHitbox_crouch.disabled = false
				if velocity.x < -((walk_speed+run_max_speed)/2):
					velocity.x = -((walk_speed+run_max_speed)/2)
			elif velocity.x < -((walk_speed+run_max_speed)/2):
				state_machine.travel("Sprint")
				
		# if abs(velocity.x) < 0.5:
		#	$StepSound.playing = false
			

	if not is_on_floor() and velocity.y > 0.1:
		state_machine.travel("Fall")
	if not is_on_floor() and velocity.y < 0.1:
		state_machine.travel("Jump")


func _physics_process(delta):
	get_input()
	velocity.y += gravity*delta
	if velocity.y >= terminal_velocity:
		velocity.y = terminal_velocity
	if jumping and is_on_floor():
		jumping = false
	velocity = move_and_slide(velocity, Vector2(0, -1))
	dcount = dcount +1
	if logger and dcount == 6:
		autologger()
		dcount = 0
	var collider = 0
	if get_slide_count() > 0:
		for i in range(get_slide_count()):
				if "EtsaiaA" in get_slide_collision(i).collider.name:
					dead()
	if is_female == false:
		$Sprite.set_texture(male)
	else:
		$Sprite.set_texture(female)
	if $DeadTimer.is_stopped() == false:
		i = i + 10*delta
		$Sprite.modulate = koloreak[int(i)%len(koloreak)]
	else:
		i = 0;
		$Sprite.modulate = "#ebfefefd"


var is_dead = false
func dead():
	if $DeadTimer.is_stopped():
		bizitza = bizitza - 10
		if bizitza <=0:
			is_dead = true
			health = health -1
			bizitza = 100
			get_tree().get_root().get_node("Node2D/Player").position = get_tree().get_root().get_node("Node2D/Hasiera").position
			state_machine.travel("Dead")
		get_tree().get_root().get_node("Node2D/Interface/HealthDisplay").update_healthbar(bizitza)
		if health <=0:
			get_tree().change_scene("res://Menu/gameover.tscn")

			#$HealthDisplay.update_healthbar(10)
			#$PlayerHitbox_standard.disabled = true
		$DeadTimer.start()

func fall():
		bizitza = 0
		if bizitza <=0:
			is_dead = true
			health = health -1
			bizitza = 100
			get_tree().get_root().get_node("Node2D/Player").position = get_tree().get_root().get_node("Node2D/Hasiera").position
			state_machine.travel("Dead")
		get_tree().get_root().get_node("Node2D/Interface/HealthDisplay").update_healthbar(bizitza)
		if health <=0:
			get_tree().change_scene("res://Menu/gameover.tscn")

		$DeadTimer.start()

func autologger():
	print("LOG:")
	print("VSpeed = ", velocity.y)
	print("HSpeed = ", velocity.x)
	#print("Right: ", rcount, ". Left: ", lcount)
	print("TotalSpeed = ", velocity.length())
	print("Animation: ", state_machine.get_current_node())
	print("Floor? ", is_on_floor())
	print("Ceiling? ", is_on_ceiling())
	print("Wall? ", is_on_wall())
	#print("Wall_jump active:", wall_jump)
	#print("Jump started? ", jump_started)
	print("Standard hitbox: ", $PlayerHitbox_crouch.disabled, "; Crouch hitbox: ", $PlayerHitbox_standard.disabled)
	print("-----------------------")
